//
//  SecondViewController.m
//  Laboratorium02
//
//  Created by Jacek Pietras on 23/11/2018.
//  Copyright © 2018 student. All rights reserved.
//

#import "SecondViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface SecondViewController ()
- (IBAction)graj:(id)sender;
- (IBAction)stop:(id)sender;
- (IBAction)graj2:(id)sender;
- (IBAction)graj3:(id)sender;

@property (nonatomic) AVAudioPlayer *player;
@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.title = @"Dzwieki";
}


- (IBAction)graj:(id)sender {
    
    NSLog(@"%ld",(long)[sender tag]);
    NSString *path = [[NSBundle mainBundle] pathForResource:@"ambient-loop" ofType:@"mp3"];
    
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: path];
    
    self.player=[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:NULL];
    
    [self.player play];
}

- (IBAction)stop:(id)sender {
    [self.player stop];
}

- (IBAction)graj2:(id)sender {
    NSLog(@"%ld",(long)[sender tag]);
    NSString *path = [[NSBundle mainBundle] pathForResource:@"missile-launch-2" ofType:@"wav"];
    
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: path];
    
    self.player=[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:NULL];
    
    [self.player play];
}

- (IBAction)graj3:(id)sender {
    NSLog(@"%ld",(long)[sender tag]);
    NSString *path = [[NSBundle mainBundle] pathForResource:@"woosh-slide-in" ofType:@"wav"];
    
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: path];
    
    self.player=[[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:NULL];
    
    [self.player play];
}

@end

