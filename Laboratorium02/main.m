//
//  main.m
//  Laboratorium02
//
//  Created by Jacek Pietras on 23/11/2018.
//  Copyright © 2018 student. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
