//
//  AppDelegate.h
//  Laboratorium02
//
//  Created by Jacek Pietras on 23/11/2018.
//  Copyright © 2018 student. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

