//
//  FirstViewController.m
//  Laboratorium02
//
//  Created by Jacek Pietras on 23/11/2018.
//  Copyright © 2018 student. All rights reserved.
//

#import "GaleriaViewController.h"

@interface GaleriaViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *obrazek;
- (IBAction)poprzedni:(id)sender;
- (IBAction)nastepny:(id)sender;


@end

@implementation GaleriaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.title = @"Galeria";
}

NSInteger test = 0;

-(NSArray *) listaObrazkow
{
    return @[@"eti_logo.png", @"pg_logo.png", @"wne.jpg", @"logo.jpg"];
}


- (IBAction)poprzedni:(id)sender {
    if (test == -1) {
        test = [[self listaObrazkow] count]-1;
    }
    UIImage *img = [UIImage imageNamed:[self listaObrazkow][test--]];
    self.obrazek.image = img;
}

- (IBAction)nastepny:(id)sender {
    if (test == [[self listaObrazkow] count]) {
        test = 0;
    }
    UIImage *img = [UIImage imageNamed:[self listaObrazkow][test++]];
    self.obrazek.image = img;
}

@end
